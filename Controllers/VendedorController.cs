// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Mvc;
// using tech_test_payment_api.Context;
// using tech_test_payment_api.Entities;

// namespace tech_test_payment_api.Controllers
// {
//     [ApiController]
//     [Route("[controller]")]
//     public class VendedorController : ControllerBase
//     {
//         private readonly VendaContext _context;

//         public VendedorController(VendaContext context)
//         {
//             _context = context;
//         }

//         #region POST Itens
//         [HttpPost("CadastroVendedor")]
//         public IActionResult Criar(Vendedor vendedor)
//         {
//             if (ValidaInclusao(vendedor))
//                 return BadRequest(new { Erro = "O item não pode ser vazio" });

//             _context.Add(vendedor);
//             _context.SaveChanges();
//             return Ok(vendedor);
//         }    
//         #endregion POST Itens



//         #region Métodos Privados
         
//         private bool ValidaInclusao (Vendedor vendedor){
//         if (vendedor == null ||
//             (vendedor.Cpf == "" || vendedor.Cpf == "string") ||
//             (vendedor.Nome == "" || vendedor.Nome == "string") ||
//             (vendedor.Telefone == "" || vendedor.Telefone == "string"))
//             {
//                 return true;
//             };
//         return false;
//          }   

//         #endregion Métodos Privados
//     }

// }