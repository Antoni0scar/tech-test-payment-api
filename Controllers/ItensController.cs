// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Mvc;
// using tech_test_payment_api.Context;
// using tech_test_payment_api.Entities;

// namespace tech_test_payment_api.Controllers
// {
//     [ApiController]
//     [Route("[controller]")]
//     public class ItensController : ControllerBase
//     {
//         private readonly VendaContext _context;

//         public ItensController(VendaContext context)
//         {
//             _context = context;
//         }

//         #region POST Itens
//         [HttpPost("CadastroItem")]
//         public IActionResult Criar(Itens item)
//         {
//             if (ValidaInclusao(item))
//                 return BadRequest(new { Erro = "O item não pode ser vazio" });

//             _context.Add(item);
//             _context.SaveChanges();
//             return Ok(item);
//         }    
//         #endregion POST Itens

        
//         #region Métodos Privados
         
//         private bool ValidaInclusao (Itens item){
//         if (item == null ||
//             (item.Nome == "" || item.Nome == "string") ||
//             (item.Valor == 0 || item.Valor.ToString() == ""))
//             {
//                 return true;
//             };
//         return false;
//          }   

//         #endregion Métodos Privados
//     }
// }