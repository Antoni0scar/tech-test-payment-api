using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        #region GET
         [HttpGet("ObtemVendaPorId/{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if  (venda == null)
                return NotFound();
            return Ok(venda);
        }
        #endregion GET

        #region POST
        [HttpPost("InclusaoVenda")]
        public IActionResult Criar(Venda venda)
        {
            if (venda == null)
                return BadRequest(new { Erro = "O item não pode ser vazio" });

            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }    
        #endregion POST
        #region PUT
        [HttpPut("AtualizaStatusVenda{idVenda}/{status}")]
        public IActionResult AtualizarStatus(int idVenda, EnumStatusVenda status)
        {
            var vendaBanco = _context.Vendas.Find(idVenda);

            if (vendaBanco == null)
                return NotFound();
            var statusVendaBanco = vendaBanco.Status;
            if (vendaBanco.AtualizaStatus(statusVendaBanco,status))
            {
                vendaBanco.Status = status;
                vendaBanco.Data = vendaBanco.Data;
                vendaBanco.Itens = vendaBanco.Itens;
                vendaBanco.Vendedor = vendaBanco.Vendedor;
                vendaBanco.Id = vendaBanco.Id;
                _context.Vendas.Update(vendaBanco);
                return Ok(vendaBanco);
            }

            return BadRequest(new { Erro = "Atualização de status não permitida." });
        }
        #endregion PUT
    }
}