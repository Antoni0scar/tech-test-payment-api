using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {

        public DateTime Data { get; set; }
        public int Id { get; set; }   
        public List<Itens> Itens { get; set; }
        public EnumStatusVenda Status { get; set; }
        public Vendedor Vendedor { get; set; }


        public bool AtualizaStatus(EnumStatusVenda statusVenda, EnumStatusVenda statusAtualizacao)
        {
            if (statusVenda == EnumStatusVenda.AGUARDANDO_PAGAMENTO && statusAtualizacao == EnumStatusVenda.PAGAMENTO_APROVADO)
                return true;
            if (statusVenda == EnumStatusVenda.AGUARDANDO_PAGAMENTO && statusAtualizacao == EnumStatusVenda.CANCELADA)
                return true;
            if (statusVenda == EnumStatusVenda.PAGAMENTO_APROVADO && statusAtualizacao == EnumStatusVenda.ENVIADO_PARA_TRANSPORTADORA)
                return true;
            if (statusVenda == EnumStatusVenda.PAGAMENTO_APROVADO && statusAtualizacao == EnumStatusVenda.CANCELADA)
                return true;
            if (statusVenda == EnumStatusVenda.ENVIADO_PARA_TRANSPORTADORA && statusAtualizacao == EnumStatusVenda.ENTREGUE)
                return true;
            return false;

        }
    }
}