namespace tech_test_payment_api.Entities
{
    public enum EnumStatusVenda
    {
        AGUARDANDO_PAGAMENTO,
        PAGAMENTO_APROVADO,
        ENVIADO_PARA_TRANSPORTADORA,
        ENTREGUE,
        CANCELADA
    }
}